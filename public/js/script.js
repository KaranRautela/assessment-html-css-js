let products;

let saveforlater = [];


const getProducts = () => {

    return fetch("http://localhost:3000/products")

        .then((result) => {

            if (result.status == 200) {

                return Promise.resolve(result.json());

            } else {

                return Promise.reject("Unable to retrieve the product list");

            }

        }).then(resultProducts => {

            products = resultProducts;

            //Populate into the DOM

            createProductList();

            return products;

        }).catch(error => {

            throw new Error(error);

        })

}

//Get the Saveforlater list

function getSaveForLater() {

    //API call

    return fetch("http://localhost:3000/saveforlater").then((result) => {

        if (result.status == 200) {

            return Promise.resolve(result.json());

        } else {

            return Promise.reject("Error");

        }

    }).then(result => {

        saveforlater = result;

        createSaveForLaterList();

        return result;

    }).catch(error => {

        throw new Error(error);

    })




}




function addProduct(id) {
    if (!isProductPresentInSaveForLater(id)) {

        let productObject = getProductById(id)

        saveforlater.push(productObject);
        //Add product call

        return fetch("http://localhost:3000/saveforlater", {

            method: 'POST',

            body: JSON.stringify(productObject),

            headers: {

                'Content-Type': 'application/json',

                'Accept': 'application/json'

            }

        }).then((result) => {

            if (result.status == 200 || result.status == 201) {

                return Promise.resolve(saveforlater);

            } else {
                return Promise.reject("Product is already saved for Later");

            }

        }).then((saveforlaterResult) => {

            createSaveForLaterList();

            return saveforlaterResult;

        })
    } 
    else {

        alert("Product is already saved for later");

        // throw new Error("Movie is already added to favourites");

    }
}




function isProductPresentInSaveForLater(selectedProductId) {

    for (let item in saveforlater) {

        if (selectedProductId == saveforlater[item].id) {

            return true;

        }

    }

    return false;

}




function getProductById(id) {
    for (let product in products) {

        if (id == products[product].id) {

            return products[product];

        }

    }

}




const createProductList = () => {

    let domProductList = '';

    products.forEach(element => {

    domProductList = domProductList + `<div class="col-md-10">
    <div class="card">
        <div class="d-flex justify-content-between p-3">
          <p class="lead mb-0">${element.brand}</p>
        </div>
        <img src="${element.thumbnail}"
          class="card-img-top" alt="Laptop" />
        <div class="card-body">
          <div class="d-flex justify-content-between">
            <p class="small"><a href="#!" class="text-muted">${element.category}</a></p>
          </div>

          <div class="d-flex justify-content-between mb-3">
            <h5 class="mb-0">${element.title}<span><small>(${element.rating})</small></span></h5>
            <h5 class="text-dark mb-0">$999</h5>
          </div>
          <p class="Description">${element.description}</p>

          <div class="d-flex justify-content-between mb-2">
            <p class="text-muted mb-0">Available: <span class="fw-bold">${element.stock}</span></p>
          </div>
          <div class="d-flex justify-content-center">
         <button onclick="addProduct(${element.id})" type="button" class="btn btn-primary">SaveForLater</button><br>
        </div>
        </div>
      </div>
    </div><br>`;

    });

    document.getElementById("products").innerHTML = domProductList;

}




const createSaveForLaterList = () => {

    let domSaveForLaterList = '';

    let childNode = document.getElementById("saveforlater");

    childNode.innerHTML = '';

    saveforlater.forEach(element => {

    domSaveForLaterList = domSaveForLaterList + `<div class="col-md-10">
    <div class="card">
        <div class="d-flex justify-content-between p-3">
          <p class="lead mb-0">${element.brand}</p>
        </div>
        <img src="${element.thumbnail}"
          class="card-img-top" alt="Laptop" />
        <div class="card-body">
          <div class="d-flex justify-content-between">
            <p class="small"><a href="#!" class="text-muted">${element.category}</a></p>
          </div>

          <div class="d-flex justify-content-between mb-3">
            <h5 class="mb-0">${element.title}<span><small>(${element.rating})</small></span></h5>
            <h5 class="text-dark mb-0">$999</h5>
          </div>
          <p class="Description">${element.description}</p>

          <div class="d-flex justify-content-between mb-2">
            <p class="text-muted mb-0">Available: <span class="fw-bold">${element.stock}</span></p>
          </div>
          <div class="d-flex justify-content-center">
          <button onclick="removeProduct(${element.id})" type="button" class="btn btn-danger">Remove from SaveForLater</button><br>
        </div>
          </div>
      </div>
    </div><br>`;


    });

    childNode.innerHTML = domSaveForLaterList;

}




function removeProduct(id) {

    saveforlater = saveforlater.filter(item => item.id !== id);

    fetch('http://localhost:3000/saveforlater/' + id, {

        method: 'DELETE',

    })

        .then((result) => {

            if (result.status == 200) {

                createSaveForLaterList();
                alert("Deleted");

            } else {
                throw new Error("unable to remove")
            }

        })

        .catch(error => {
            throw new Error(error);

        });

}

getProducts();

getSaveForLater();


